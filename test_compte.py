from compte import CompteCourant,CompteSimple,Banque
import pytest

def test_crediter():
    c = CompteSimple('Xavier',250)
    c.crediter(150)
    assert c.solde == 400

def test_debiter():
    c = CompteSimple('Xavier',250)
    c.debiter(150)
    assert c.solde == 100

def test_debiter_neg():
    c = CompteSimple('Xavier',250)
    c.debiter (300)
    assert c.solde == -50

def test_solde_caché():
    with pytest.raises(AttributeError):
        c = CompteSimple('Xavier',250)
        c.solde -= 300
        assert c.solde == -50

def test_compte_courant():
    c = CompteCourant('Alexandre',350)
    c.crediter(250)
    assert c.solde == 600
    assert c.releve == ["+250"]
    c.debiter(700)
    assert c.solde == -100
    assert c.releve ==["+250","-700"]
    c.crediter(200)
    c.crediter(400)
    c.debiter(100)
    c.editer_releve()

def test_banque():
    b = Banque()
    c1 = b.ouvrir_compte_simple('Xavier',300)
    c2 = b.ouvrir_compte_courant('Alexandre',500)
    c3 = b.ouvrir_compte_simple('Florent',200)
    c4 = b.ouvrir_compte_courant('Damien',600)
    assert b.total_soldes() == 1600
    b.prelevement_frais(400)
    assert b.total_soldes() == 0
    b.editer_releves()

def test_id():
    b = Banque()
    c1 = b.ouvrir_compte_simple('Xavier',300)
    n = CompteSimple._CompteSimple__id -1
    c2 = b.ouvrir_compte_courant('Alexandre',500)
    c3 = b.ouvrir_compte_simple('Florent',200)
    c4 = b.ouvrir_compte_courant('Damien',600)
    assert c1.id == n
    assert c2.id == n+1
    assert c4.id == n+3

