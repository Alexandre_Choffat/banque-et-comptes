class CompteSimple():
    '''
    Un compte banquaire avec un solde et un titulaire.
    '''
    __id=10001
    def __init__(self,titulaire,solde = 0):
        self.__solde = solde
        self.titulaire = titulaire
        self.id = self.__id
        CompteSimple.__id += 1
    
    @property
    def solde(self):
        return(self.__solde)
    
    def crediter(self,montant):
        if montant > 0 :
            self.__solde += montant
        else :
            raise("Montant négatif")
    
    def debiter(self,montant):
        if montant > 0 :
            self.__solde -= montant
        else :
            raise("Montant négatif")

class Banque() :
    '''
    Une banque qui gère des comptes pour ses utilisateurs.
    '''
    def __init__(self):
        self.comptes = []

    def ouvrir_compte_simple(self, titulaire,solde =0):
        c = CompteSimple(titulaire,solde)
        self.comptes.append(c)
        return(c)
    
    def ouvrir_compte_courant(self,titulaire,solde = 0):
        c = CompteCourant(titulaire,solde)
        self.comptes.append(c)
        return(c)
    
    def total_soldes(self):
        total = 0
        for c in self.comptes :
            total += c.solde
        return(total)

    def prelevement_frais(self,frais):
        for c in self.comptes : 
            c.debiter(frais)
    
    def editer_releves(self):
        for c in self.comptes :
            if isinstance(c,CompteCourant):
                print(f"Titulaire du compte : {c.titulaire}")
                c.editer_releve()

class CompteCourant(CompteSimple):
    ''' 
    Compte qui permet d'éditer un relevé des opérations et le solde.
    '''
    def __init__(self,titulaire,solde=0):
        super().__init__(titulaire,solde)
        self.releve = []
    
    #Ajout de la gestion du releve dans les opérations sur le solde.
    def crediter(self,montant):
        super().crediter(montant)
        self.releve.append(montant))

    def debiter(self,montant):
        super().debiter(montant)
        self.releve.append(-1*montant))
    
    def editer_releve(self):
        print(f'Relevé des opérations du compte {self.id}')
        for operation in self.releve :
            print(operation)
        print(f'Solde actuel : {self.solde}')